package com.nati.desafio.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CursoTelaController {

	@GetMapping("/listaCurso")
	public String lista() {
		return "curso/lista";
	}
	
	@GetMapping("/cadastroCurso/**")
	public String cadastro() {
		return "curso/cadastro";
	}
	
}
