package com.nati.desafio.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DisciplinaTelaController {

	@GetMapping("/listaDisciplina")
	public String lista() {
		return "disciplina/lista";
	}
	
	@GetMapping("/cadastroDisciplina/**")
	public String cadastro() {
		return "disciplina/cadastro";
	}
	
}
