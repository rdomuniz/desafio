package com.nati.desafio.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MatrizTelaController {

	@GetMapping({"/listarMatrizManutecao","/listarMatrizVisualizacao"})
	public String listar() {
		return "matriz/lista";
	}
	
	@GetMapping({"/mantemMatriz/**","/visualizaMatriz/**"})
	public String cadastro() {
		return "matriz/cadastro";
	}
	
}