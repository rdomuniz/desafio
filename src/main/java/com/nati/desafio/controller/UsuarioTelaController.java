package com.nati.desafio.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UsuarioTelaController {

	@GetMapping("/listaUsuario")
	public String lista() {
		return "usuario/lista";
	}
	
	@GetMapping("/cadastroUsuario/**")
	public String cadastro() {
		return "usuario/cadastro";
	}
	
}
