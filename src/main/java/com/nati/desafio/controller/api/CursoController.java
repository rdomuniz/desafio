package com.nati.desafio.controller.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nati.desafio.model.Curso;
import com.nati.desafio.service.CursoService;

@RestController()
@RequestMapping("/api")
public class CursoController extends GeralController{

	@Autowired
	private CursoService service;
	
	@GetMapping("/cursos")
	public List<Curso> cursos() {
		return service.listar();
	}
	
	@PostMapping("/cursos")
	public Curso criar(@Valid @RequestBody Curso curso) {
		return service.criar(curso);
	}
	
	@PutMapping("/cursos")
	public Curso atualizar(@Valid @RequestBody Curso curso) {
		return service.atualizar(curso);
	}
	
	@GetMapping("/cursos/{id}")
	public Curso burcar(@PathVariable(value="id") Long id) {
		return service.burcar(id);
	}
	
	@DeleteMapping("/cursos/{id}")
	public ResponseEntity<?> excluir(@PathVariable(value="id") Long id) {
		service.excluir(id);
		return ResponseEntity.ok().build();
	}
	
}
