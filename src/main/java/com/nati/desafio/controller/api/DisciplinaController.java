package com.nati.desafio.controller.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nati.desafio.model.Disciplina;
import com.nati.desafio.service.DisciplinaService;

@RestController
@RequestMapping("/api")
public class DisciplinaController extends GeralController{

	@Autowired
	private DisciplinaService service;
	
	@GetMapping("/disciplinas")
	public List<Disciplina> cursos() {
		return service.listar();
	}
	
	@PostMapping("/disciplinas")
	public Disciplina criar(@Valid @RequestBody Disciplina disciplina) {
		return service.criar(disciplina);
	}
	
	@PutMapping("/disciplinas")
	public Disciplina atualizar(@Valid @RequestBody Disciplina disciplina) {
		return service.atualizar(disciplina);
	}
	
	@GetMapping("/disciplinas/{id}")
	public Disciplina burcar(@PathVariable(value="id") Long id) {
		return service.burcar(id);
	}
	
	@DeleteMapping("/disciplinas/{id}")
	public ResponseEntity<?> excluir(@PathVariable(value="id") Long id) {
		service.excluir(id);
		return ResponseEntity.ok().build();
	}
	
}
