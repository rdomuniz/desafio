package com.nati.desafio.controller.api;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nati.desafio.model.DisciplinaCurso;
import com.nati.desafio.service.DisciplinaCursoService;

@RestController
@RequestMapping("/api")
public class DisciplinaCursoController extends GeralController{

	@Autowired
	private DisciplinaCursoService service;
	
	@PostMapping("/disciplinaCurso")
	public void criar(@Valid @RequestBody DisciplinaCurso disciplinaCurso) {
		service.criar(disciplinaCurso);
	}
	
	@DeleteMapping("/disciplinaCurso/{id}")
	public void excluir(@PathVariable(value="id") Long id) {
		service.excluir(id);
	}
	
}