package com.nati.desafio.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nati.desafio.model.Curso;
import com.nati.desafio.model.DisciplinaCurso;
import com.nati.desafio.service.CursoService;

import lombok.Data;

@RestController()
@RequestMapping("/api")
public class MatrizController extends GeralController{

	@Autowired
	private CursoService service;
	
	@GetMapping("/matriz/{id}")
	public CursoView burcar(@PathVariable(value="id") Long id) {
		return montaView(service.burcar(id));
	}
	
	private CursoView montaView(Curso curso) {
		CursoView cursoView = new CursoView();
		cursoView.setId(curso.getId());
		cursoView.setNome(curso.getNome());
		cursoView.setQuantidadeDeSemestres(curso.getQuantidadeDeSemestres());
		
		HashMap<Integer, SemestreView> map = new HashMap<>();
		for (int i = 1; i <= curso.getQuantidadeDeSemestres(); i++) {
			SemestreView semestreView = new SemestreView();
			semestreView.setSemeste(i);
			cursoView.getSemestres().add(semestreView);
			map.put(semestreView.getSemeste(), semestreView);
		}
		
		for (DisciplinaCurso disciplinaCurso : curso.getDisciplinas()) {
			SemestreView semestreView = map.get(disciplinaCurso.getSemestre());
			DisciplinaView disciplinaView = new DisciplinaView();
			disciplinaView.setIdRelacional(disciplinaCurso.getId());
			disciplinaView.setId(disciplinaCurso.getDisciplina().getId());
			disciplinaView.setNome(disciplinaCurso.getDisciplina().getNome());
			disciplinaView.setCreditos(disciplinaCurso.getDisciplina().getCreditos());
			semestreView.getDisciplinas().add(disciplinaView);
		}
		
		return cursoView;
	}
	
	@Data
	public class DisciplinaView {
		private Long idRelacional;
		private Long id;
		private String nome;
		private Integer creditos;
	}
	
	@Data
	public class SemestreView {
		private Integer semeste;
		private List<DisciplinaView> disciplinas = new ArrayList<DisciplinaView>();
	}

	@Data
	public class CursoView {
		private Long id;
		private String nome;
		private Integer quantidadeDeSemestres;
		private List<SemestreView> semestres = new ArrayList<SemestreView>();
	}
	
	
	
}