package com.nati.desafio.controller.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nati.desafio.model.Usuario;
import com.nati.desafio.service.UsuarioService;

@RestController()
@RequestMapping("/api")
public class UsuarioController extends GeralController{

	@Autowired
	private UsuarioService service;
	
	@GetMapping("/usuarios")
	public List<Usuario> usuarios() {
		return service.listar();
	}
	
	@PostMapping("/usuarios")
	public Usuario criar(@Valid @RequestBody Usuario usuario) {
		return service.criar(usuario);
	}
	
	@PutMapping("/usuarios")
	public Usuario atualizar(@Valid @RequestBody Usuario usuario) {
		return service.atualizar(usuario);
	}
	
	@GetMapping("/usuarios/{id}")
	public Usuario burcar(@PathVariable(value="id") Long id) {
		return service.burcar(id);
	}
	
	@DeleteMapping("/usuarios/{id}")
	public ResponseEntity<?> excluir(@PathVariable(value="id") Long id) {
		service.excluir(id);
		return ResponseEntity.ok().build();
	}
	
}
