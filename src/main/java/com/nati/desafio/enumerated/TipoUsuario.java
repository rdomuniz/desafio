package com.nati.desafio.enumerated;

public enum TipoUsuario {

	ADMINISTRADOR,
	GERENTE_CURSO,
	PROFESSOR,
	ALUNO;
	
}
