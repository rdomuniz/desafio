package com.nati.desafio.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
public class Disciplina implements Serializable {
	
	private static final long serialVersionUID = -6964174556543493025L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotBlank(message="Nome deve ser informado!")
	private String nome;
	
	@Min(message="Quantidade deve ser mair que 0", value=1)
	@NotNull(message="Quantidade de semestres deve ser informado!")
	private Integer creditos;

}
