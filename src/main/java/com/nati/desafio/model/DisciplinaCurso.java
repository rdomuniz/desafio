package com.nati.desafio.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity(name="disciplina_curso")
public class DisciplinaCurso implements Serializable {
	
	private static final long serialVersionUID = -6964174556543493025L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JsonIgnore
	@NotBlank(message="Curso deve ser informado!")
	@JoinColumn(foreignKey=@ForeignKey(name="curso_id"))
	private Curso curso;
	
	@ManyToOne
	@NotBlank(message="Disciplina deve ser informado!")
	@JoinColumn(foreignKey=@ForeignKey(name="disciplina_id"))
	private Disciplina disciplina;
	
	@Min(message="O semestre deve ser mair que 0", value=1)
	@NotNull(message="Quantidade de semestres deve ser informado!")
	private Integer semestre;
	
}