package com.nati.desafio.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.nati.desafio.enumerated.TipoUsuario;

import lombok.Data;

@Data
@Entity
public class Usuario implements Serializable {

	private static final long serialVersionUID = 407893501209242970L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotBlank(message="Nome deve ser informado!")
	private String nome;

	@NotBlank(message="Login deve ser informado!")
	private String login;
	
	private String senha;

	@Transient
	private String confirmacao;

	@NotNull(message="Tipo deve ser informado!")
	@Enumerated(EnumType.STRING)
	private TipoUsuario tipo;
	
}
