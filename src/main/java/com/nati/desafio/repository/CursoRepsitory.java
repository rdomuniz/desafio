package com.nati.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nati.desafio.model.Curso;

public interface CursoRepsitory extends JpaRepository<Curso, Long> {

}
