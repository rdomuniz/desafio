package com.nati.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nati.desafio.model.DisciplinaCurso;

public interface DisciplinaCursoRepsitory extends JpaRepository<DisciplinaCurso, Long> {

}
