package com.nati.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nati.desafio.model.Disciplina;

public interface DisciplinaRepsitory extends JpaRepository<Disciplina, Long> {

}
