package com.nati.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nati.desafio.model.Usuario;

public interface UsuarioRepsitory extends JpaRepository<Usuario, Long> {

}
