package com.nati.desafio.service;

import java.util.List;

import com.nati.desafio.model.Curso;

public interface CursoService extends GeralService {

	public List<Curso> listar();
	public Curso criar(Curso curso);
	public Curso atualizar(Curso curso);
	public Curso burcar(Long id);
	public void excluir(Long id);
	
}
