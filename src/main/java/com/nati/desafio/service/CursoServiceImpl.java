package com.nati.desafio.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nati.desafio.model.Curso;
import com.nati.desafio.repository.CursoRepsitory;

@Service
public class CursoServiceImpl implements CursoService {

	@Autowired
	private CursoRepsitory repsitory;

	@Override
	public List<Curso> listar() {
		return repsitory.findAll();
	}
	
	@Override
	public Curso criar(Curso curso) {
		return repsitory.save(curso);
	}

	@Override
	public Curso atualizar(Curso curso) {
		repsitory.findById(curso.getId()).orElseThrow(()->new RuntimeException("Curso não existe!"));
		return repsitory.save(curso);
	}

	@Override
	public Curso burcar(Long id) {
		return repsitory.findById(id).orElseThrow(()->new RuntimeException("Curso não existe!"));
	}

	@Override
	public void excluir(Long id) {
		repsitory.findById(id).orElseThrow(() -> new RuntimeException("Curso não existe!"));
		repsitory.deleteById(id);
	}

}