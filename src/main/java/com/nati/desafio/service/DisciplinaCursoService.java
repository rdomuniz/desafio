package com.nati.desafio.service;

import com.nati.desafio.model.DisciplinaCurso;

public interface DisciplinaCursoService extends GeralService {

	public void criar(DisciplinaCurso disciplinaCurso);
	public void excluir(Long id);
	
}
