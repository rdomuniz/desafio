package com.nati.desafio.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nati.desafio.model.DisciplinaCurso;
import com.nati.desafio.repository.DisciplinaCursoRepsitory;

@Service
public class DisciplinaCursoServiceImpl implements DisciplinaCursoService {

	@Autowired
	private DisciplinaCursoRepsitory repsitory;

	@Override
	public void criar(DisciplinaCurso disciplinaCurso) {
		repsitory.save(disciplinaCurso);
	}

	@Override
	public void excluir(Long id) {
		repsitory.findById(id).orElseThrow(() -> new RuntimeException("Disciplina não relacionada a este curso!"));
		repsitory.deleteById(id);
	}

}