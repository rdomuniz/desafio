package com.nati.desafio.service;

import java.util.List;

import com.nati.desafio.model.Disciplina;

public interface DisciplinaService extends GeralService {

	public List<Disciplina> listar();
	public Disciplina criar(Disciplina disciplina);
	public Disciplina atualizar(Disciplina disciplina);
	public Disciplina burcar(Long id);
	public void excluir(Long id);
	
}