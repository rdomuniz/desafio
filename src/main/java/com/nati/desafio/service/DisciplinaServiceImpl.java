package com.nati.desafio.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nati.desafio.model.Disciplina;
import com.nati.desafio.repository.DisciplinaRepsitory;

@Service
public class DisciplinaServiceImpl implements DisciplinaService {

	@Autowired
	private DisciplinaRepsitory repsitory;

	@Override
	public List<Disciplina> listar() {
		return repsitory.findAll();
	}
	
	@Override
	public Disciplina criar(Disciplina disciplina) {
		return repsitory.save(disciplina);
	}

	@Override
	public Disciplina atualizar(Disciplina disciplina) {
		repsitory.findById(disciplina.getId()).orElseThrow(()->new RuntimeException("Disciplina não existe!"));
		return repsitory.save(disciplina);
	}

	@Override
	public Disciplina burcar(Long id) {
		return repsitory.findById(id).orElseThrow(()->new RuntimeException("Disciplina não existe!"));
	}

	@Override
	public void excluir(Long id) {
		repsitory.findById(id).orElseThrow(() -> new RuntimeException("Disciplina não existe!"));
		repsitory.deleteById(id);
	}

}