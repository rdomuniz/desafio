package com.nati.desafio.service;

import java.util.List;

import com.nati.desafio.model.Usuario;

public interface UsuarioService extends GeralService {

	public List<Usuario> listar();
	public Usuario criar(Usuario usuario);
	public Usuario atualizar(Usuario usuario);
	public Usuario atualizarSenha(Usuario usuario);
	public Usuario burcar(Long id);
	public void excluir(Long id);
	
}
