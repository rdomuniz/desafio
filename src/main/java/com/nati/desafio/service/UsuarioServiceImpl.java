package com.nati.desafio.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.nati.desafio.model.Usuario;
import com.nati.desafio.repository.UsuarioRepsitory;
import com.nati.desafio.util.GeradorDeHashMd5;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepsitory repsitory;

	@Override
	public List<Usuario> listar() {
		return repsitory.findAll();
	}
	
	@Override
	public Usuario criar(Usuario usuario) {
		processarSenha(usuario);
		return repsitory.save(usuario);
	}

	@Override
	public Usuario atualizar(Usuario usuario) {
		Usuario usuarioBase = repsitory.findById(usuario.getId()).orElseThrow(()->new RuntimeException("Usuário não existe!"));
		usuario.setSenha(usuarioBase.getSenha());
		return repsitory.save(usuario);
	}
	
	@Override
	public Usuario atualizarSenha(Usuario usuario) {
		repsitory.findById(usuario.getId()).orElseThrow(()->new RuntimeException("Usuário não existe!"));
		processarSenha(usuario);
		return repsitory.save(usuario);
	}

	@Override
	public Usuario burcar(Long id) {
		return repsitory.findById(id).orElseThrow(()->new RuntimeException("Usuário não existe!"));
	}

	@Override
	public void excluir(Long id) {
		repsitory.findById(id).orElseThrow(() -> new RuntimeException("Usuário não existe!"));
		repsitory.deleteById(id);
	}
	
	private void processarSenha(Usuario usuario) {
		if(StringUtils.isEmpty(usuario.getSenha()))
			throw new RuntimeException("Senha não informada!");
		if(StringUtils.isEmpty(usuario.getConfirmacao()))
			throw new RuntimeException("Confimação de senha não informada!");
		if(usuario.getSenha().compareTo(usuario.getConfirmacao()) != 0)
			throw new RuntimeException("Confimação de senha diferente da informada");
		usuario.setSenha(new GeradorDeHashMd5().gera(usuario.getSenha()));
	}

}