package com.nati.desafio.util;

import java.security.MessageDigest;

public class GeradorDeHashMd5 {

	/**
	 * Gera chave md5 para um dado valor.
	 */
	public String gera(String value) {
		try {
			MessageDigest algorithm = MessageDigest.getInstance("MD5");
			byte messageDigest[] = algorithm.digest(value.getBytes("UTF-8"));
			return toHexadecimal(messageDigest);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Erro ao gerar chave md5.");
		}
	}

	private String toHexadecimal(byte[] messageDigest) {
		String hash;
		StringBuilder hexString = new StringBuilder();
		for (byte b : messageDigest) {
			hexString.append(String.format("%02X", 0xFF & b));
		}
		hash = hexString.toString().toLowerCase();
		return hash;
	}
	
}
