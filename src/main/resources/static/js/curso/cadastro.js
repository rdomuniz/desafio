var id;
var edicao = false;
jQuery(document).ready(function () {
	var splitURL = window.location.href.split("/");
	if(splitURL.length > 4){
		edicao = true;
		jQuery.ajax({
			url: "/api/cursos/"+splitURL[4],
			type: "GET",
			dataType: "json",
			success: function(data, textStatus, jqXHR) {
				$("#id").val(data.id);
				$("#nome").val(data.nome);
				$("#quantidadeDeSemestres").val(data.quantidadeDeSemestres);
			},
			error: function(xhr, status, error) {
				jQuery.xhrErrorMessage(xhr);
			}
		});
	}
	jQuery("#botaoSalvar").click(function() {
		jQuery.salvar();
	});
//	jQuery.showErrorMessage("teste");
});

jQuery.salvar = function () {
	var metodo = 'POST';
	if(edicao)
		metodo = 'PUT';
	
	var dados = {};
	dados.id = $("#id").val();
	dados.nome = $("#nome").val();
	dados.quantidadeDeSemestres = $("#quantidadeDeSemestres").val();
	jQuery.ajax({
		url: "/api/cursos",
		type: metodo,
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(dados),
		success: function(data, textStatus, jqXHR) {
			jQuery.showSuccessMessage("Operação realizada com sucesso",function(){
				window.location = "/listaCurso";
			});
		},
		error: function(xhr, status, error) {
			jQuery.xhrErrorMessage(xhr);
		}
	});
};