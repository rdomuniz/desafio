jQuery(document).ready(function () {
	jQuery.pesquisar();
});

jQuery.pesquisar = function () {
	jQuery.ajax({
		url: "/api/disciplinas",
		type: 'GET',
		dataType: "json",
		success: function(data, textStatus, jqXHR) {
			var tabela = $("#tabela");
			tabela.find(".dados").remove();
			jQuery.each(data, function(index, value){
				var tr = $("<tr/>");
				tr.addClass("dados");
				tr.append($("<td/>").css("text-align","center").append(value.id));
				tr.append($("<td/>").css("text-align","left").append(value.nome));
				tr.append($("<td/>").css("text-align","right").append(value.creditos));
				tr.append($("<td/>").css("text-align","center").append(
					$("<a/>").click(function() {
						window.location = "/cadastroDisciplina/"+value.id;
					}).append($("<i/>").addClass("icon-pencil2"))
				).append("&nbsp;").append(
					$("<a/>").click(function() {
						jQuery.showConfirmationMessage("Deseja realmente excluir esta disciplina?",function(result){
							if(result){
								jQuery.ajax({
									url: "/api/disciplinas/"+value.id,
									type: "DELETE",
									success: function(data, textStatus, jqXHR) {
										jQuery.showSuccessMessage("Operação realizada com sucesso",function(){
											jQuery.pesquisar();
										});
									},
									error: function(xhr, status, error) {
										jQuery.xhrErrorMessage(xhr);
									}
								});
							}
						});
					}).append($("<i/>").addClass("icon-remove"))
				));
				tabela.append(tr);
			});
		},
		error: function(xhr, status, error) {
			
		}
	});
}