String.prototype.replaceAll = function(target, replacement) {
  return this.split(target).join(replacement);
};

jQuery.showSuccessMessage = function(msg,callback) {
	$('.bootbox').modal('hide');
	bootbox.alert({ 
	    size: 'small',
	    title: "Desafio", 
	    message: '<i class="alerta-icone icon-checkmark-circle"></i><span class="alerta-msg">' + msg.replaceAll("\n","<br/>") + '</span>',
	    buttons: {
	        'ok': {
	            label: 'Ok',
	            className: 'btn-principal botaoOK'
	        }
	    }
	});
	$('.bootbox').on('hidden.bs.modal', function () {
		if( callback ) callback();
	});	
};

var resultShowConfirmacao = false;
jQuery.showConfirmationMessage = function(msg,callback){
	$('.bootbox').modal('hide');
	var element = bootbox.confirm({ 
//	    size: 'small',
	    title: "Desafio",
	    message: '<i class="alerta-icone icon-question"></i><span class="alerta-msg">' + msg.replaceAll("\n","<br/>") + '</span>',
	    buttons: {
	        'cancel': {
	            label: 'Não',
	            className: 'btn-default'
	        },
	        'confirm': {
	            label: 'Sim',
	            className: 'btn-principal botaoOK'
	        }
	    },
	    callback: function(result){
	    	resultShowConfirmacao = result;
	    }
	});
	$('.bootbox').on('hidden.bs.modal', function() {
		if( callback ) callback(resultShowConfirmacao);
	});
	resultShowConfirmacao = false;
};

jQuery.showErrorMessage = function(msg,callback) {
	$('.bootbox').modal('hide');
	bootbox.alert({ 
		title: "Desafio",
//	    size: 'small',
	    message: '<i class="alerta-icone icon-warning"></i><span class="alerta-msg">' + msg.replaceAll("\n","<br/>") + '</span>',
	    buttons: {
	        'ok': {
	            label: 'Ok',
	            className: 'btn-principal botaoOK'
	        }
	    }
	});
	$('.bootbox').on('hidden.bs.modal', function () {
		if( callback ) callback();
	});	
};

jQuery.xhrErrorMessage = function(xhr) {
	if(xhr.responseJSON.errors){
		var msg = "";
		jQuery.each(xhr.responseJSON.errors, function(index, value){
			if(msg != "")
				msg += "&nbsp;";
			msg += value.defaultMessage;
		});
	} else if(xhr.responseJSON.message){
		msg = xhr.responseJSON.message;
	} else {
		msg = "Erros inesperado!"
	}
	jQuery.showErrorMessage(msg);
};