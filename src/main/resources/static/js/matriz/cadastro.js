var id;
jQuery(document).ready(function () {
	var splitURL = window.location.href.split("/");
	jQuery.ajax({
		url: "/api/matriz/"+splitURL[4],
		type: "GET",
		dataType: "json",
		success: function(data, textStatus, jqXHR) {
			$("#id").val(data.id);
			$("#nome").val(data.nome);
			$("#quantidadeDeSemestres").val(data.quantidadeDeSemestres);
			var matriz = $("#matriz");
			jQuery.each(data.semestres, function(index, value){
				matriz.append($("<h5/>").html("Semestre " + value.semeste));
			});
			var modalSemestre = $("#modalSemestre");
			for (i = 1; i <= data.quantidadeDeSemestres; i++){
				modalSemestre.append($("<option/>").val(i).html("Semestre " + i));
			}
		},
		error: function(xhr, status, error) {
			jQuery.xhrErrorMessage(xhr);
		}
	});
	jQuery.ajax({
		url: "/api/disciplinas",
		type: 'GET',
		dataType: "json",
		success: function(data, textStatus, jqXHR) {
			var modalDisciplina = $("#modalDisciplina");
			jQuery.each(data, function(index, value){
				modalDisciplina.append($("<option/>").val(value.id).html(value.nome));
			});
		},
		error: function(xhr, status, error) {
			jQuery.xhrErrorMessage(xhr);
		}
	});
	jQuery("#botaoIncluir").click(function() {
		var popupAtiva = jQuery('#modalIncluirDisciplina');
		popupAtiva.on('shown.bs.modal', function (e) {
		});
		popupAtiva.on('hidden.bs.modal', function (e) {
		});
		popupAtiva.modal();
	});
	jQuery("#botaoSalvar").click(function() {
	});
});