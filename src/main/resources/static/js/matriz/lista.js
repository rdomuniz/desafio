var manutencao = false;
jQuery(document).ready(function () {
	if(window.location.href.split("/")[3] == "listarMatrizManutecao")
		manutencao = true;
	jQuery.pesquisar();
});

jQuery.pesquisar = function () {
	jQuery.ajax({
		url: "/api/cursos",
		type: 'GET',
		dataType: "json",
		success: function(data, textStatus, jqXHR) {
			var tabela = $("#tabela");
			tabela.find(".dados").remove();
			jQuery.each(data, function(index, value){
				var tr = $("<tr/>");
				tr.addClass("dados");
				tr.append($("<td/>").css("text-align","center").append(value.id));
				tr.append($("<td/>").css("text-align","left").append(value.nome));
				tr.append($("<td/>").css("text-align","right").append(value.quantidadeDeSemestres));
				tr.append($("<td/>").css("text-align","center").append(
					$("<a/>").click(function() {
						window.location = (manutencao?"/mantemMatriz/":"/visualizaMatriz/")+value.id;
					}).append($("<i/>").addClass((manutencao?"icon-pencil2":"icon-file3")))
				));
				tabela.append(tr);
			});
		},
		error: function(xhr, status, error) {
			
		}
	});
}