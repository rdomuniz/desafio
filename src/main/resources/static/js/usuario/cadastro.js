var id;
var edicao = false;
jQuery(document).ready(function () {
	var splitURL = window.location.href.split("/");
	if(splitURL.length > 4){
		edicao = true;
		jQuery.ajax({
			url: "/api/usuarios/"+splitURL[4],
			type: "GET",
			dataType: "json",
			success: function(data, textStatus, jqXHR) {
				$("#id").val(data.id);
				$("#nome").val(data.nome);
				$("#tipo").val(data.tipo);
				$("#login").val(data.login);
			},
			error: function(xhr, status, error) {
				jQuery.xhrErrorMessage(xhr);
			}
		});
		$("#linhaSenha").remove();
	} else {
		$("#login").removeAttr("readonly");
		$("#linhaSenha").show();
	}
	jQuery("#botaoSalvar").click(function() {
		jQuery.salvar();
	});
//	
});

jQuery.salvar = function () {
	var metodo = 'POST';
	if(edicao)
		metodo = 'PUT';
	var dados = {};
	dados.id = $("#id").val();
	dados.nome = $("#nome").val();
	if($("#tipo").val() != "")
		dados.tipo = $("#tipo").val();
	dados.login = $("#login").val();
	dados.senha = $("#senha").val();
	dados.confirmacao = $("#confirmacao").val();
	jQuery.ajax({
		url: "/api/usuarios",
		type: metodo,
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(dados),
		success: function(data, textStatus, jqXHR) {
			jQuery.showSuccessMessage("Operação realizada com sucesso",function(){
				window.location = "/listaUsuario";
			});
		},
		error: function(xhr, status, error) {
			jQuery.xhrErrorMessage(xhr);
		}
	});
};